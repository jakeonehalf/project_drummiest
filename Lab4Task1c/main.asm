;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
; Task 1 C
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------

			; Timer_A Control Register
			bis.w #0200h, &TA1CTL			; Set clock to SMCLK
			bis.w #0010h, &TA1CTL			; Set to UP mode

			; Timer_A Compare/Capture Control Register 1
			bis.w #00C0h, &TA1CCTL1			; Set to Toggle/Set

			mov.w #03E7h, &TA1CCR0			; Set top to 999 (counts to 1000)

			mov.w #012Bh, &TA1CCR1			; Toggle when count reaches 299.

			bis.b #BIT2, &P2DIR				; Set pin 1.0 to output
			bis.b #BIT2, &P2SEL				; Output PWM signal

											; Loop forever
LOOP		jmp LOOP						; or until the heat death of the universe.

;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect 	.stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
