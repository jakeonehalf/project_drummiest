import cv2
import numpy as np
from pdfconvert import pdf_to_png

"""
	Created By: 		Jake Thiem
	Last Edited By:		Jake Thiem
	Last Edit Date:		November 6, 2015
"""

class Read:
	"""
	This is the main class for the OMR
	"""
	def __init__(self):
		filename = raw_input("Insert PDF filename without the extension: ")	
		self.filename = filename
		self.pages = pdf_to_png(filename)

	def read_music(self):
		for x in range(0, self.pages):
			im_gray = cv2.imread(self.filename + "-" + str(x) + ".png", cv2.CV_LOAD_IMAGE_GRAYSCALE)
			cv2.im
			# im_gray_mat = cv2.fromarray(im_gray)
			# cv2.imshow(im_gray)
			# im_bw = cv.CreateImage(cv.GetSize(im_gray_mat), cv.IPL_DEPTH_8U, 1);
			# im_bw_mat = cv.GetMat(im_bw)
			# cv2.Threshold(im_gray_mat, im_bw_mat, 0, 255, cv2.CV_THRESH_BINARY | cv2.CV_THRESH_OTSU);
			# cv2.imshow('', np.asarray(im_bw_mat))
			# cv2.waitKey()


if __name__ == '__main__':
	r = Read()
	r.read_music()